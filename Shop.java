import java.util.Scanner;
public class Shop {
	public static void main(String[] args){
		Scanner keyboard = new Scanner(System.in);
		Dress[] dresses = new Dress[4];
		for(int i=0; i < 4; i++){
			dresses[i] = new Dress();
			System.out.println("Enter the price of the dress: ");
			dresses[i].price = Integer.parseInt(keyboard.nextLine());
			System.out.println("Enter the color of the dress: ");
			dresses[i].color = keyboard.nextLine();
			System.out.println("Enter the length of the dress: ");
			dresses[i].length = keyboard.nextLine();
		}
		System.out.println("The last dress is:  "+dresses[3].price+"$, it is "+dresses[3].color+", and it is "+dresses[3].length);
		
		dresses[3].discount();
		System.out.println("After the discount, the last dress is:  "+dresses[3].price+"$, it is "+dresses[3].color+", and it is "+dresses[3].length);
		
	}
}

